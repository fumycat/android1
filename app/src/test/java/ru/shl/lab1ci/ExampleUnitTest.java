package ru.shl.lab1ci;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void min_a1_b2_exp1() {
        assertEquals(1, Utils.min(1, 2));
    }
    @Test
    public void min_a3_b2_exp2() {
        assertEquals(2, Utils.min(3, 2));
    }
    @Test
    public void max_a3_b4_exp4() {
        assertEquals(4, Utils.max(3, 4));
    }
    @Test
    public void max_a4_b1_exp4() {
        assertEquals(4, Utils.max(4, 1));
    }
}