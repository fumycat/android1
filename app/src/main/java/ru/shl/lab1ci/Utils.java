package ru.shl.lab1ci;

public class Utils {
    /**
     * <p>Сравнивает два числа.</p>
     *
     * @param a - первое числа
     * @param b - второе числа
     * @return a или b, в зависимсоти от того, какое числа меньше
    */
    public static Number min(Number a, Number b) {
        return a.doubleValue() < b.doubleValue() ? a : b;
    }
    /**
     * <p>Сравнивает два числа.</p>
     *
     * @param a - первое числа
     * @param b - второе числа
     * @return a или b, в зависимсоти от того, какое числа больше
     */
    public static Number max(Number a, Number b) {
        return a.doubleValue() > b.doubleValue() ? a : b;
    }
}
